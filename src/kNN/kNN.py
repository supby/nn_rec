import numpy as np
import operator

class KNN(object):
    
    __data = None
    __k = None
    __dataSetSize = 0
    __labels = None
    
    def __init__(self, data, labels, k):
        self.__data = data
        self.__labels = labels
        self.__k = k
        self.__dataSetSize = np.shape(data)[0]
        
    def classify(self, vector):
        diffMat = np.tile(vector, (self.__dataSetSize, 1)) - self.__data
        sqDiffMat = np.power(diffMat, 2)
        sqDistances = sqDiffMat.sum(axis=1)
        distances = np.power(sqDistances, 0.5)
        hashTable = {}
        for i in range(self.__dataSetSize):
            hashTable[distances[i][0,0]] = self.__labels[i]
                        
        sortedDistances = np.sort(distances, axis=0)        
        classCount = {}  
        for i in range(self.__k):
            voteLabel = hashTable[sortedDistances[i][0,0]]
            classCount[voteLabel] = classCount.get(voteLabel, 0) + 1
        
        sortedClassCount = sorted(classCount.iteritems(), key=operator.itemgetter(1), reverse = True)
        return sortedClassCount[0][0]