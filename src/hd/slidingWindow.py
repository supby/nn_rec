'''
Created on 10.09.2012

@author: HP625
'''
from PIL import Image
from DataLoader import DataLoader
import NN
import Utils
import numpy

if __name__ == '__main__':
    pass

im = Image.open("../../data/test/swtest1gr.bmp")
im = Image.eval(im, lambda(x):255-x)

w,h = im.size

Th = DataLoader().LoadWeidts("../../res/w.dat")
Th1 = Th[0]
Th2 = Th[1]
foundNumbers = []
for i in range(0,h/9):
    for j in range(0,w/3):
        tj = j*3
        ti = i*3
        crop = im.crop((tj, ti, tj + 28, ti + 28))
        buf = [255] + list(crop.getdata())
        
        #img = Image.frombuffer('L',(28,28), buf,'raw','L',0,1)
        #img = Image.eval(img, lambda(x):255-x)    
        #img.save("e:/ptmp/o1/nnrec" + str(i) + str(j) + ".jpg")
        #crop.save("e:/ptmp/o1/nnrec" + str(i) + str(j) + ".jpg")
        
        testScaledData = Utils.featuresScaling([buf], NN.MAX_FEATURE_VALUE)
                
        fdt = NN.forwardProp([Th1, Th2], testScaledData[0])
        z3t,a3t = fdt[1]        
        maxV = max(a3t)
        if maxV >= 0.8:
            fNum = a3t.index(maxV)
            if not foundNumbers.__contains__(fNum):
                print "max=", maxV            
                print fNum
                foundNumbers.append(fNum)
        #else:
            #print "none"