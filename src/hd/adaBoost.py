'''
Created on 20.11.2012

@author: HP625
'''

from shared import DataLoader
from shared import Utils
import marshal
import pickle
import time
import copy
from adaboost import adaboostLib

if __name__ == '__main__':
    pass



dl = DataLoader.DataLoader()

#trainCount = 100
#rawData,featuresCount,trainingSetCount = dl.GetTrainingDataSet("../../data/train-images.idx3-ubyte", trainCount)
#Y = dl.GetLabels("../../data/train-labels.idx1-ubyte", trainCount)
#
#scaledData = Utils.featuresScaling(rawData, 255)
##print scaledData
#classCount = 10
#allWeakClassArr = []
#for i in range(0, classCount):
#    start = time.time()
#    
#    iY = copy.deepcopy(Y)    
#    iY[iY != i] = -1.0
#    iY[iY == i] = 1.0
##    print iY.T
##    print Y.T
##    break
#    allWeakClassArr.append(adaBoostTrainDS(scaledData, iY.T))    
#    print "Class ", i, " trained, ", (time.time() - start), "s."
##print allWeakClassArr
#with open("../../res/boostTrained.dat", "wb") as f:    
#    pickle.dump(allWeakClassArr, f)

##test

allWeakClassArr = []    
with open("../../res/hd_boost_60000.dat", "rb") as f:
    allWeakClassArr = pickle.load(f)
print allWeakClassArr

testCount = 1000

rawDataTest,featuresCountTest,testSetCount = dl.GetTrainingDataSet("../../data/t10k-images.idx3-ubyte", testCount)
testY = dl.GetLabels("../../data/t10k-labels.idx1-ubyte", testCount)

scaledDataTest = Utils.featuresScaling(rawDataTest, 255)

failures = 0
singleCount = 0
searchedDigit = 1
for i in range(0,testCount):        
        weakClassArr = allWeakClassArr[searchedDigit]
        predictVal = adaboostLib.adaClassify(scaledDataTest[i], weakClassArr)[0]
        if(int(testY[i]) == searchedDigit):
            singleCount = singleCount + 1
        if(predictVal == 1.0 and int(testY[i]) != searchedDigit):
            failures = failures + 1
#    for cln in range(0, len(allWeakClassArr)):
#        weakClassArr = allWeakClassArr[cln]
#        predictVal = adaboostLib.adaClassify(scaledDataTest[i], weakClassArr)[0]
#        if(predictVal == 1.0 and int(testY[i]) != cln):
#            failures = failures + 1
#print "FailuresCount=", failures,"fauluresPer=",float(float(failures)/float(testCount)*100),"%"
print "FailuresCount=", failures,"fauluresPer=",float(float(failures)/float(singleCount)*100),"%"
print "exCount=", singleCount






