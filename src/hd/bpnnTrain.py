'''
Created on 14.08.2012

@author: HP625
'''

#from PIL import Image

from shared import DataLoader
from nn import BPNN
import numpy
from shared import Utils

if __name__ == '__main__':
    pass


# trainCount = 60000
trainCount = 60000
dl = DataLoader.DataLoader()    
rawData,featuresCount,trainingSetCount = dl.GetTrainingDataSet("../../data/train-images.idx3-ubyte", trainCount)
Y = dl.GetLabels("../../data/train-labels.idx1-ubyte", trainCount)

output = []
for i in range(0, trainCount):
    ly = numpy.zeros((10), numpy.ubyte)
    ly[Y[i]] = 1
    output.append(ly)

bpnn1 = BPNN.BPNN([784, 800, 10])
bpnn1.learn(rawData, output, 1000, 0.1, 0.1, 1)

# dl.SaveWeidths("../../res/hd_it1000_lr01_conf784_800_10.dat", bpnn1.getWeidths())
dl.SaveWeidths("../../res/tmp1.dat", bpnn1.getWeidths())

#Th = dl.LoadWeidts("../../res/wn60k_rate07_epoch1000.dat")
#Th = dl.LoadWeidts("../../res/w.dat")
#
## test 1
#testCount = 10000
#rawDataTest,featuresCountTest,testSetCount = dl.GetTrainingDataSet("../../data/t10k-images.idx3-ubyte", testCount)
#testY = dl.GetLabels("../../data/t10k-labels.idx1-ubyte")
#
#testScaledData = Utils.featuresScaling(rawDataTest, 255)
#
#failures = 0
#
#bpnn1 = BPNN([784, 50, 10])
#bpnn1.setWeidth(Th)
#for i in range(0,testCount):
#
#    fdt = bpnn1.run(testScaledData[i])
#    z3t,a3t = fdt[1]
#    maxV = max(a3t)
#    if(int(testY[i]) != a3t.index(maxV)):
#        failures = failures + 1    
#    #print "L=",testY[i],"H=",a3t.index(max(a3t))
#print "FailuresCount=", failures,"fauluresPer=",float(float(failures)/float(testCount)*100),"%"





             

