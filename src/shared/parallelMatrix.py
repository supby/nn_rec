import time
import numpy
import threading
import numpy.distutils.system_info as sysinfo

BLOCK_COUNTS = 8
CORE_COUNTS = 6

pool_sema = threading.BoundedSemaphore(value=CORE_COUNTS)

class UnionThread (threading.Thread):
    def __init__(self, result_mat, block_mat, block_i, block_j):
        threading.Thread.__init__(self)
        
        self.result_mat = result_mat        
        self.block_mat = block_mat
        self.block_i = block_i
        self.block_j = block_j
          
    def run(self):
#         print "Starting Union " + self.name
        self.union_wrk(self.result_mat, self.block_mat, self.block_i, self.block_j)
#         print "Exiting Union" + self.name
        
    def union_wrk(self, result_mat, block_mat, block_i, block_j):
        pool_sema.acquire()
        
        block_rows_num, block_cols_num = numpy.shape(block_mat)
        for k in range(block_rows_num):
            for n in range(block_cols_num):
                c_row = block_i*(block_rows_num - (0 if block_rows_num % 2 == 0 else 1)) + k
                c_col = block_j*(block_cols_num - (0 if block_cols_num % 2 == 0 else 1)) + n
                result_mat[c_row, c_col] = block_mat[k,n]
    
        pool_sema.release()

class DotThread (threading.Thread):
    def __init__(self, kw, ab_cols_num, bb_cols_num, ab, bb, cb):
        threading.Thread.__init__(self)
        self.kw = kw
        self.ab_cols_num = ab_cols_num
        self.bb_cols_num = bb_cols_num
        self.ab = ab
        self.bb = bb
        self.cb = cb
          
    def run(self):
#         print "Starting " + self.name
        self.dot_wrk(self.kw, self.ab_cols_num, self.bb_cols_num, self.ab, self.bb, self.cb)
#         print "Exiting " + self.name

    def dot_wrk(self, row_cursor, ab_cols_num, bb_cols_num, ab, bb, cb):
        pool_sema.acquire()
        for i in range(bb_cols_num):
            for j in range(ab_cols_num): # bb_rows_num == ab_cols_num
                cb[row_cursor,i] += numpy.dot(ab[row_cursor,j], bb[j,i])
        pool_sema.release()

def dot(a, b):
    
    if sysinfo.get_info('atlas') or sysinfo.get_info('blas'):
        return numpy.dot(a,b)
    
    a_shape = numpy.shape(a)
    b_shape = numpy.shape(b)
#     print 'a = ({0}), b = ({1})'.format(a_shape, b_shape)
    
    assert a_shape[1] == b_shape[0]
    
    c = numpy.zeros((a_shape[0], 1 if len(b_shape) == 1 else b_shape[1]), numpy.float)
    
    now = time.time()
    
    ab = splitMatrix(a, BLOCK_COUNTS)
    bb = splitMatrix(b, BLOCK_COUNTS)
    
    print 'splitted {0}'.format(time.time() - now)
    now = time.time()
    
    ab_rows_num, ab_cols_num = numpy.shape(ab)
    bb_rows_num, bb_cols_num = numpy.shape(bb)
    
#     print 'ab = ({0}), bb = ({1})'.format((ab_rows_num, ab_cols_num), (bb_rows_num, bb_cols_num))
    
    cb = numpy.zeros((ab_rows_num, bb_cols_num), numpy.object)
    
    assert(ab_cols_num == bb_rows_num)
    
    ths = []
    for k in range(ab_rows_num):
        th = DotThread(k, ab_cols_num, bb_cols_num, ab, bb, cb)
        th.start()
        ths.append(th)
          
    for th in ths:
        th.join()
    
    print 'dotted {0}'.format(time.time() - now)
    now = time.time()
    
    ths_u = []
    for i in range(ab_rows_num):
        for j in range(bb_cols_num):
            th_u = UnionThread(c, cb[i,j], i, j)
            th_u.start()
            ths_u.append(th_u)
    
    for thi in ths_u:
        thi.join()
        
    print 'united {0}'.format(time.time() - now)
    
    c_shape = numpy.shape(c)
    if c_shape[0] == 1 or c_shape[1] == 1:
        c = numpy.asarray(c).reshape(-1)
    
    return c


def splitMatrix(mat, blockCounts):
    
    mat_shape = numpy.shape(mat)
    m = numpy.matrix(mat)
    if len(mat_shape) == 1: # it is vector
        m = m.T        
     
    rowsNum, colsNum = numpy.shape(m)
        
    rowsDivider = blockCounts / 2
    colsDivider = blockCounts / 2
        
    rowsNumBlock = int(rowsNum/rowsDivider)
    colsNumBlock = int(colsNum/colsDivider)
    
    if rowsNumBlock == 0:
        rowsNumBlock = 1
    if colsNumBlock == 0:
        colsNumBlock = 1
    
    blocksNumH = int(rowsNum/rowsNumBlock)
    blocksNumH_recent = rowsNum % rowsNumBlock
    
    blocksNumW = int(colsNum/colsNumBlock)
    blocksNumW_recent = colsNum % colsNumBlock
    
    ret = numpy.zeros((blocksNumH, blocksNumW), numpy.object)

    for i in range(0, blocksNumH):
        startRow = i*rowsNumBlock
        for j in range(0, blocksNumW):
            startCol = j*colsNumBlock
            ret[i,j] = m[startRow:startRow + rowsNumBlock + (blocksNumH_recent if blocksNumH_recent > 0 and i == blocksNumH - 1 else 0),
                         startCol:startCol + colsNumBlock + (blocksNumW_recent if blocksNumW_recent > 0 and j == blocksNumW - 1 else 0)]
    return ret