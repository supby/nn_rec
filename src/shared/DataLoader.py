'''
Created on 15.08.2012

@author: HP625
'''

import numpy
import struct
import time

class DataLoader(object):
    '''
    classdocs
    '''
    def __init__(self):
        '''
        Constructor
        '''
        
    def GetTrainingDataSet(self, idxFilename, readCount=0):
        with open(idxFilename, "rb") as imgFile:    
            magicNum =  struct.unpack(">I", imgFile.read(4))[0]
            trainingSetCount =  int(struct.unpack(">I", imgFile.read(4))[0])
            if readCount > 0:
                trainingSetCount = readCount
            rowsCount = struct.unpack(">I", imgFile.read(4))[0]
            colsCount = struct.unpack(">I", imgFile.read(4))[0]
            
            featuresCount = rowsCount*colsCount
            print "trainingSetCount=",trainingSetCount,"featuresCount=",featuresCount    
                
            print "Reading trainig data set..."
            start = time.time()
            X = numpy.zeros((trainingSetCount, featuresCount), numpy.ubyte)
            bytes_read = imgFile.read(featuresCount)
            i = 0
            while bytes_read and i < trainingSetCount:        
                    X[i] = [struct.unpack("B", b)[0] for b in bytes_read]                     
                    bytes_read = imgFile.read(featuresCount)
                    i = i + 1
            
            imgFile.close()
        
        print "Done. ",(time.time() - start)," s."
        return (X,featuresCount,trainingSetCount)
    
    def GetLabels(self, idxFilename, readCount=0):
        with open(idxFilename, "rb") as lblFile:
            magicNum =  struct.unpack(">I", lblFile.read(4))[0]
            labelsCount =  struct.unpack(">I", lblFile.read(4))[0] 
            if readCount > 0:
                labelsCount = readCount  
            
            print "labelsCount=",labelsCount        
                
            print "Reading labels data set..."
            start = time.time()
            Y = numpy.zeros((labelsCount, 1), numpy.int)
            byte_read = lblFile.read(1)
            i = 0
            while byte_read and i < labelsCount:        
                    Y[i] = struct.unpack("B", byte_read)[0] 
                    byte_read = lblFile.read(1)
                    i = i + 1
            
            lblFile.close()
            
        print "Done. ",(time.time() - start)," s."
        return Y
    
    def SaveWeidths(self, filename, Th):
        with open(filename, "wb") as f:
            for i in range(0,len(Th)):
                r,c = numpy.shape(Th[i])
                f.write(struct.pack("I", r))
                f.write(struct.pack("I", c))
                for j in range(0,r):
                    for k in range(0,c):
                        f.write(struct.pack("f", float(Th[i][j][k])))
    
    def LoadWeidts(self, filename):
        ThArr = []
        with open(filename, "rb") as f:            
            while True:
                rb = f.read(4)
                if not rb:
                    break
                r = int(struct.unpack("I", rb)[0])                
                c = int(struct.unpack("I",f.read(4))[0])
                Th = numpy.zeros((r,c), numpy.float)
                for j in range(0,r):
                    for k in range(0,c):
                        w = float(struct.unpack("f",f.read(4))[0])
                        Th[j][k] = w
                        
                ThArr.append(Th)
        return ThArr
            
                
        