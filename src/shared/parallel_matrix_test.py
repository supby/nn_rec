'''
Created on Oct 18, 2013

@author: andrej
'''
import time
import numpy
import parallelMatrix

a = numpy.random.random((1000,1000))
b = numpy.random.random((1000,1000))
# a = numpy.random.random((5, 4))
# b = numpy.random.random((4, 5))
# a = numpy.random.random((4, 5))
# b = numpy.random.random((5, 4))

# a = numpy.mat([[1,2,3,4,5],
#                [1,2,3,4,5],
#                [1,2,3,4,5],
#                [1,2,3,4,5]], numpy.int32)
# b = numpy.mat([[1,2,3,4],
#                [1,2,3,4],
#                [1,2,3,4],
#                [1,2,3,4],
#                [1,2,3,4]], numpy.int32)

# asp = parallelMatrix.splitMatrix(a, 4)
# r,c = numpy.shape(asp)
# for i in range(r):
#     for j in range(c):
#         print '-----------', '(i={0}, j={1})'.format(i,j)
#         print asp[i,j]


now = time.time()

c = parallelMatrix.dot(a,b) 
# print c
print '----------- done. p ----------- {0}'.format(time.time() - now)

now = time.time()

nc = numpy.dot(a,b)
# print nc
print '----------- done. numpy ----------- {0}'.format(time.time() - now)

c_shape = numpy.shape(c)
nc_shape = numpy.shape(nc)

assert c_shape == nc_shape
print '----------- shapes are equal -----------'

if len(c_shape) == 1:
    for i in range(c_shape[0]):
        assert round(c[i], 5) == round(nc[i], 5)
else:
    for i in range(c_shape[0]):
        for j in range(c_shape[1]):
            assert round(c[i,j], 5) == round(nc[i,j], 5)
        
print '----------- matrixes are equal -----------'
print 'DONE.'




