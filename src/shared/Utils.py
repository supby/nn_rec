'''
Created on 16.08.2012

@author: HP625
'''
import numpy
import math

def featureNormalizationMat(data):
    mat = numpy.mat(data, numpy.float)
    minV = numpy.min(mat)
    maxV = numpy.max(mat)
    r = maxV - minV    
    
    return numpy.divide(numpy.subtract(mat, minV), r)

def featureNormalization(featureVector):
    maxV = numpy.max(featureVector)
    minV = numpy.min(featureVector)
    r = maxV - minV
    
    l = len(featureVector)
    scaledVector = numpy.zeros((l), numpy.float)
    for i in range(0,l):
        scaledVector[i] = float((featureVector[i] - minV))/r
        
    return scaledVector

def matrixRand(matrix):
    rowsCount = len(matrix)
    if(rowsCount == 0):
        return
    colCount = len(matrix[0])
    for i in range(0, rowsCount):
        for j in range(0, colCount):
            matrix[i][j] = numpy.random.uniform(-0.7,0.7)
            
def featuresScaling(matrix, maxValue):
    return numpy.divide(matrix, float(maxValue))

def sigmoid(val):
  return (1.0/(1.0 + math.exp(-float(val))))
        
def sigmoidArr(arr):
  return [sigmoid(x) for x in arr]        

def sigmoidGradient(val):
    return sigmoid(val)*(1.0 - sigmoid(val))

def sigmoidGradientArr(arr):
    return [sigmoidGradient(x) for x in arr]