'''
Created on 14.04.2013

@author: HP625
'''

from numpy import *

def loadDataSet(fileName, delim='\t'):
    fr=open(fileName)
    stringArr=[line.strip().split(delim) for line in fr.readlines()]
    datArr=[map(float,line) for line in stringArr]
    return mat(datArr)

def pca(dataMat, topNfeat=9999999):
    meanVals = mean(dataMat, axis=0)
    meanRemoved = dataMat - meanVals    
    covMat = cov(meanRemoved, rowvar=0)
    eigVals,sigVects = linalg.eig(mat(covMat))
    eigValInd = argsort(eigVals)
    eigValInd = eigValInd[:-(topNfeat+1):-1]
    redEigVets = sigVects[:,eigValInd]
    lowDDataMat = meanRemoved * redEigVets
    reconMat = (lowDDataMat*redEigVets.T) + meanVals    
    return lowDDataMat, reconMat     
