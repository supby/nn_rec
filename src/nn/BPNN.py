'''
Created on 02.09.2012

@author: HP625
'''
import time
import numpy
import math
from shared import parallelMatrix

class BPNNOptions:
    useAdaptiveLearaningRate = False
    adaptiveLRateStep = 0.1
    useFeatureScaling = True

class BPNN:
    
    confArr = []
    Th = []
    maxFeatureValue = 0
    options = None
    
    def __init__(self, confArr, options=BPNNOptions()):
        self.confArr = confArr
        self.options = options
        
    def getWeidths(self):
        return self.Th
    
    def setWeidth(self, Th):
        self.Th = Th;
    
    def addBiasAll(self, data):
        featuresCount = self.confArr[0]
        bRawData = numpy.zeros((len(data),featuresCount + 1), numpy.float)
        for i in range(0, len(data)):
            bRawData[i][0] = 1 
            bRawData[i][1:] = data[i]
        return bRawData
    
    def run(self, x):
        featuresCount = self.confArr[0]
        bx = numpy.zeros((featuresCount + 1), numpy.float)
        bx[0] = 1 
        bx[1:] = x
        return self.forwardProp(bx)
        
    def forwardProp(self, x):
        ret = []
        a = x
        for i in range(0, len(self.Th)):
            z = parallelMatrix.dot(self.Th[i], a)
            a = self.sigmoidArr(z)
            if i < (len(self.Th) - 1):
                a = numpy.insert(a, 0, 1)
            ret.append((z,a))
        return ret
    
    def sigmoidArr(self, arr):
        return [self.sigmoid(x) for x in arr]        

    def sigmoidGradient(self, val):
        return self.sigmoid(val)*(1.0 - self.sigmoid(val))
    
    def sigmoidGradientArr(self, arr):
        return [self.sigmoidGradient(x) for x in arr]
    
    def sigmoid(self, val):
        return (1.0/(1.0 + math.exp(-float(val))))
    
    def costFunction(self, Th, scaledData, Y, trainingSetCount, featuresCount, lam):
#         print '----- cf start -----'
        J = 0.0
#        print Y
        outputLen = self.confArr[-1]
        lLen = len(self.confArr)
        Theta_grads = []
        for i in range(0, len(Th)):
            Theta_grads.append(numpy.matrix(numpy.zeros(numpy.shape(Th[i]), numpy.float)))
                    
        for i in range(0, trainingSetCount):
            
            fd = self.forwardProp(scaledData[i])
            fdLen = len(fd)
                            
            ly = numpy.array(Y[i])

            # cost(error) function
            for j in range(0, outputLen):
                J = J + ((-1)*ly[j]*math.log(fd[-1][1][j]) - (1 - ly[j])*math.log(1 - fd[-1][1][j]))        
             
            # sigma for output layer
            dsOut = fd[-1][1] - ly
#             print '----- cf 0 -----'
            # sigma for hidden layers
            dsPrev = dsOut         
            ds = [dsOut]
            for j in range(1, lLen - 1):
                dst = numpy.multiply(parallelMatrix.dot(numpy.transpose(Th[lLen - 1 - j]), dsPrev)[1:][0], 
                                 self.sigmoidGradientArr(fd[fdLen - 1 - j][0]))
                ds.insert(0, dst)
                dsPrev = dst
#             print '----- cf 1 -----'
            a = scaledData[i]
            for j in range(0, len(Theta_grads)):     
#                 print 'shp1, shp2 = {0}, {1}'.format(numpy.shape(numpy.transpose(numpy.matrix(ds[j]))),numpy.shape(numpy.matrix(a)))       
                Theta_grads[j] = Theta_grads[j] + parallelMatrix.dot(numpy.transpose(numpy.matrix(ds[j])), numpy.matrix(a))
                a = fd[j][1]
#             print '----- cf 2 -----'
        
#         print '----- cf 3 -----'    
        # compute regularizaion
        tmp = []
        for i in range(0, len(Th)):
            temp = Th[i]
            for j in range(0,len(temp)):
                temp[j][0] = 0
            tmp.append(temp)
        
        for i in range(0, len(tmp)):
            Theta_grads[i] = numpy.multiply(Theta_grads[i], (1.0/trainingSetCount)) + float(lam/trainingSetCount)*tmp[i]
        
        reg = 0
        for i in range(0, len(tmp)):
            reg = reg + numpy.sum(numpy.power(tmp[i], 2))
        
        reg = float(lam/(2.0*trainingSetCount))*reg
        
        J = (1.0/trainingSetCount)*J + reg
        print "J = ", J, "reg = ", reg
        
        for i in range(0, len(Theta_grads)):
            Theta_grads[i] = numpy.array(Theta_grads[i])
        
#         print '----- cf end -----'
        return (J, Theta_grads)
    
    def matrixRand(self, matrix):
        rowsCount = len(matrix)
        if(rowsCount == 0):
            return
        colCount = len(matrix[0])
        for i in range(0, rowsCount):
            for j in range(0, colCount):
                matrix[i][j] = numpy.random.uniform(-0.12,0.12)
                
    def featureNormalization(self, data):
        mat = numpy.mat(data, numpy.float)
        minV = numpy.min(mat)
        maxV = numpy.max(mat)
        r = maxV - minV
        return numpy.divide(numpy.subtract(mat, minV), r)
    
    def learn(self, input, output, maxIter, desiredErr, learnRate, lam):
        
        trainingSetCount = len(input)
        featuresCount = self.confArr[0]
        
        # Initialize weights
        
        print "Initialize weights..."
        start = time.time()
                
        for i in range(0, len(self.confArr) - 1):
            Thi = numpy.zeros((self.confArr[i + 1], self.confArr[i] + 1), numpy.float)
            self.matrixRand(Thi)
            self.Th.append(Thi)
        
        print "Done. ",(time.time() - start)," s."
                
        # Features scaling
        start = time.time()
        scaledData = input
        if(self.options.useFeatureScaling):
            print "Features scaling..."
            scaledData = self.featureNormalization(input)
            print "Done. ",(time.time() - start)," s."
        
        #    add bias in input data
        scaledData = self.addBiasAll(scaledData)
        
        INF_CONST = 10000000
        
        print "Learning..."
        start = time.time()
        if(maxIter == 0):
            maxIter = INF_CONST
#        print output
        jPrev = INF_CONST
        thPrev = None
        for i in range(0,maxIter):
            print "iter = ", i
            J, grads = self.costFunction(self.Th, scaledData, output, trainingSetCount, featuresCount, lam)            
            if(J <= desiredErr or (J > jPrev 
                                   and not self.options.useAdaptiveLearaningRate)):
                break
            
            if(self.options.useAdaptiveLearaningRate and J >= jPrev):
                newLr = learnRate - self.options.adaptiveLRateStep
                print "Increase learning rate from ", learnRate, "to ", newLr  
                learnRate = newLr
                self.Th = thPrev
                
            jPrev = J
            thPrev = self.Th
            for n in range(0, len(grads)):
                for j in range(0, len(grads[n])):
                    for k in range(0, len(grads[n][j])):
                        self.Th[n][j][k] = self.Th[n][j][k] - learnRate*grads[n][j][k]
            
        
        print "Done. ",(time.time() - start)," s."
    
    