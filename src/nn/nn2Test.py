'''
Created on 27.12.2012

@author: HP625
'''

#from PIL import Image

from shared import DataLoader
from NN2 import *
import numpy
from shared import Utils

trainCount = 10
if __name__ == '__main__':
    pass

dl = DataLoader.DataLoader()    
rawData,featuresCount,trainingSetCount = dl.GetTrainingDataSet("../../data/train-images.idx3-ubyte", trainCount)
Y = dl.GetLabels("../../data/train-labels.idx1-ubyte", trainCount)

rawData = Utils.featuresScaling(rawData, 255)

pat = []
for i in range(0, trainCount):
    ly = numpy.zeros((10), numpy.ubyte)
    ly[Y[i]] = 1
    pat.append([rawData[i], ly])

n = NN(784, 50, 10)
n.train(pat)

#dl.SaveWeidths("../../res/w.dat", bpnn1.getWeidths())

#Th = dl.LoadWeidts("../../res/w_nn_60000.dat")
#Th = dl.LoadWeidts("../../res/w.dat")

# test 1
#testCount = 10000
#rawDataTest,featuresCountTest,testSetCount = dl.GetTrainingDataSet("../../data/t10k-images.idx3-ubyte", testCount)
#testY = dl.GetLabels("../../data/t10k-labels.idx1-ubyte")
#
#testScaledData = Utils.featuresScaling(rawDataTest, 255)
#
#failures = 0
#
#bpnn1 = BPNN([784, 50, 10])
#bpnn1.setWeidth(Th)
#for i in range(0,testCount):
#
#    fdt = bpnn1.run(testScaledData[i])
#    z3t,a3t = fdt[1]
#    maxV = max(a3t)
#    if(int(testY[i]) != a3t.index(maxV)):
#        failures = failures + 1    
#    #print "L=",testY[i],"H=",a3t.index(max(a3t))
#print "FailuresCount=", failures,"fauluresPer=",float(float(failures)/float(testCount)*100),"%"
