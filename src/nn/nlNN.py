'''
Created on 22.12.2012

@author: HP625
'''


import numpy as np
import neurolab as nl


from shared import DataLoader
from shared import Utils

trCount = 100

dl = DataLoader.DataLoader()    
rawData,featuresCount,trainingSetCount = dl.GetTrainingDataSet("../../data/train-images.idx3-ubyte", trCount)
Y = dl.GetLabels("../../data/train-labels.idx1-ubyte", trCount)

scaledData = Utils.featuresScaling(rawData, 255)

NUM_LABELS = 10


nY = []
for i in range(0, len(scaledData)):
    ly = np.zeros((NUM_LABELS), np.ubyte)
    ly[Y[i]] = 1
    nY.append(ly)
    

net = nl.net.newff([[0, 1]]*featuresCount, [10, NUM_LABELS])
#print np.shape(scaledData)
#print np.shape(nY)
#print net.co

#print featuresCount
# Train process
#err = net.train(scaledData, nY, show=15)
nl.train.train_gd(net, scaledData, nY, show=15)
# Test
#net.sim([[0.2, 0.1]]) # 0.2 + 0.1

