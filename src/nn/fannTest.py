'''
Created on 25.12.2012

@author: HP625
'''

from pyfann import libfann


learning_rate = 0.7
num_layers = 3
num_input = 2
num_hidden = 2
num_output = 1

desired_error = 0.0001
max_iterations = 10
iterations_between_reports = 1

ann = libfann.neural_net()

ann.create_standard_array((num_input, num_hidden, num_output))

ann.set_learning_rate(learning_rate)

ann.set_activation_function_hidden(libfann.SIGMOID_SYMMETRIC)
ann.set_activation_function_output(libfann.SIGMOID_SYMMETRIC)

trData = libfann.training_data()
trData.set_train_data([[1,1],[1,0]],[[1],[0]])
#ann.train_on_data(trData, max_iterations, iterations_between_reports, desired_error)


#learning_rate = 0.7
#num_input = 2
#num_hidden = 4
#num_output = 1
#
#desired_error = 0.0001
#max_iterations = 100000
#iterations_between_reports = 1000
#
#ann = libfann.neural_net()
#ann.create_standard_array((num_input, num_hidden, num_output))
#
#ann.set_learning_rate(learning_rate)
#
#ann.set_activation_function_hidden(libfann.SIGMOID_SYMMETRIC)
#ann.set_activation_function_output(libfann.SIGMOID_SYMMETRIC)
#-0
#ann.train_on_file("../../data/xor.data", max_iterations, iterations_between_reports, desired_error)



