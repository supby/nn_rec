from nn import BPNN
import csv
import numpy as np
from shared import Utils
from shared import DataLoader

rawData = []
with open('../../data/kaggle/hdw_recogn/test.csv', 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    i = 0
    for row in reader:
        if(i > 0):
            rawData.append(row)
        i += 1

# print np.shape(rawData)
rawData = np.matrix(rawData, np.ubyte)
# print rawData
scaledData = Utils.featureNormalizationMat(rawData)

(testCount, featuresCount) = np.shape(scaledData)

dl = DataLoader.DataLoader()
Th = dl.LoadWeidts("../../res/hd_it1000_lr01.dat")

bpnn1 = BPNN.BPNN([784, 50, 10])
bpnn1.setWeidth(Th)
print 'testCount=', testCount

with open('../../res/kaggle/hdw_recogn/res.csv', 'wb') as csvfile:
    writer = csv.writer(csvfile)    

    for i in range(0,testCount):
        fdt = bpnn1.run(scaledData[i])
        z3t,a3t = fdt[1]
        maxV = max(a3t)
        writer.writerow([a3t.index(maxV)])
#         print a3t.index(maxV)
    
print 'Done.'