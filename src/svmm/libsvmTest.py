'''
Created on 08.10.2012

@author: HP625
'''

from svm import *
from svmutil import *

if __name__ == '__main__':
    pass


from shared import DataLoader
from shared import Utils


trainCount = 10000

dl = DataLoader.DataLoader()    
rawData,featuresCount,trainingSetCount = dl.GetTrainingDataSet("../../data/train-images.idx3-ubyte", trainCount)
Y = dl.GetLabels("../../data/train-labels.idx1-ubyte", trainCount)

scaledData = Utils.featuresScaling(rawData, 255)

classes = []
data = []
#data.append(scaledData[4])
#print data 
for i in range(0,len(scaledData)):
    di = []
    di[:] = scaledData[i]
    data.append(di)    
    classes.append(Y[i])

problem = svm_problem(classes,data)

param=svm_parameter()
#param=svm_parameter("-q")
#param.cross_validation=1
#param.nr_fold=10
#param.kernel_type=RBF

m = svm_train(problem,param)

#testing the model
#svm_predict([4], [[1,1]], m)

testCount = 100
rawDataTest,featuresCountTest,testSetCount = dl.GetTrainingDataSet("../../data/t10k-images.idx3-ubyte", testCount)
testY = dl.GetLabels("../../data/t10k-labels.idx1-ubyte")

testScaledData = Utils.featuresScaling(rawDataTest, 255)

failures = 0

for i in range(0,testCount):
    c = 0
    for j in range(0,10):
        td = []
        td[:] = testScaledData[i] 
        p_lbl, p_acc, p_prob = svm_predict([j], [td], m)
        c = p_lbl[0] 
    
    if(int(testY[i]) != c):
        failures = failures + 1    
    
print "FailuresCount=", failures,"fauluresPer=",float(float(failures)/float(testCount)*100),"%"
