'''
Created on 03.01.2013

@author: HP625

translate images to grayscale
'''
from PIL import Image


#pedestrian0999.png

prefix = "negative-road-"

imgCount = 1000

for i in range(0, imgCount):
    num = str(i).zfill(6)
    filename = prefix + num + ".png"
    im = Image.open("d:/Docs/ComputingDoc/AI/Computer Vision/train DBs/DATASET-CVC-02/CVC-02-Classification/train/negative-road-rescaled-64x128/color/" + filename)
    gim = im.convert("L")
    gim.save("d:/Dropbox/Dropbox/prj/nn_rec/data/pedestrianFixed64x128/neg/" + filename) 
    print filename, " saved."

