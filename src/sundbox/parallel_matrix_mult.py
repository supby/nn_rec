import numpy
import time
from shared import parallelMatrix

# a = [[1,2,3,4,5,6,7,8],
#      [1,2,3,4,5,6,7,8],
#      [1,2,3,4,5,6,7,8],
#      [1,2,3,4,5,6,7,8],
#      [1,2,3,4,5,6,7,8],
#      [1,2,3,4,5,6,7,8],
#      [1,2,3,4,5,6,7,8],
#      [1,2,3,4,5,6,7,8]]
#  
# b = [[1,2,3,4,5,6,7,8],
#      [1,2,3,4,5,6,7,8],
#      [1,2,3,4,5,6,7,8],
#      [1,2,3,4,5,6,7,8],
#      [1,2,3,4,5,6,7,8],
#      [1,2,3,4,5,6,7,8],
#      [1,2,3,4,5,6,7,8],
#      [1,2,3,4,5,6,7,8]]

a = numpy.random.randint(10, size=(2000,2000))
b = numpy.random.randint(10, size=(2000,2000))
 
start = time.time()

parallelMatrix.dot(a, b)

print 'mp:', time.time() - start

print '---------'
 
start = time.time()
 
pd = numpy.dot(a, b)
 
print 'np:', time.time() - start
