'''
Created on 09.01.2013

@author: HP625
'''

from svm import *
from svmutil import *

import haarExtractor
import numpy as np
import utils
import time

posPrefix = "pedestrian"
negPrefix = "not-pedestrian"

trainCount = 1000

print "fill train data"
start = time.time()

scaledData = []
labelsData = []
for i in range(0, trainCount):
    num = str(i).zfill(4)
    posFilename = posPrefix + num + ".png"
    negFilename = negPrefix + num + ".png"
    
    iimg = haarExtractor.getIntegral("d:/Dropbox/Dropbox/prj/nn_rec/data/pedestrian/pedestrian/" + posFilename)
    features = haarExtractor.extractHaarFeatures(iimg)
    scaled = utils.featureNormalization(features)
    
    di = []
    di[:] = scaled
    scaledData.append(di)
    labelsData.append(1)
    
    iimg = haarExtractor.getIntegral("d:/Dropbox/Dropbox/prj/nn_rec/data/pedestrian/not-pedestrian/" + negFilename)
    features = haarExtractor.extractHaarFeatures(iimg)
    scaled = utils.featureNormalization(features)
    di = []
    di[:] = scaled
    scaledData.append(di)
    labelsData.append(-1)
    
print "Done. ",(time.time() - start)," s."

print "Learning..."
start = time.time()

problem = svm_problem(labelsData,scaledData)

param=svm_parameter()
m = svm_train(problem,param)



print "Done. ",(time.time() - start)," s."

#testing the model
#testCount = 100
#rawDataTest,featuresCountTest,testSetCount = dl.GetTrainingDataSet("../../data/t10k-images.idx3-ubyte", testCount)
#testY = dl.GetLabels("../../data/t10k-labels.idx1-ubyte")
#
#testScaledData = Utils.featuresScaling(rawDataTest, 255)
#
#failures = 0
#
#for i in range(0,testCount):
#    c = 0
#    for j in range(0,10):
#        td = []
#        td[:] = testScaledData[i] 
#        p_lbl, p_acc, p_prob = svm_predict([j], [td], m)
#        c = p_lbl[0] 
#    
#    if(int(testY[i]) != c):
#        failures = failures + 1    
#    
#print "FailuresCount=", failures,"fauluresPer=",float(float(failures)/float(testCount)*100),"%"