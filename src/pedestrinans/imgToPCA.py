'''
Created on 14.01.2013

@author: HP625
'''

import numpy as np
import utils
import time
from shared import DataLoader
from shared import pca
import pickle
from PIL import Image

#posPrefix = "pedestrian"
#negPrefix = "not-pedestrian"

def imToMat(im):
    pix = im.load()
    w,h = im.size
    ret = np.zeros(w*h, np.byte)
    for i in range(0, h):
        for j in range(0, w):
            ret[i*w+j] = pix[j,i];
    return np.mat(ret)
#     return ret

posPrefix = "positive-"
negPrefix = "negative-road-"

pairCount = 1000 # neg + pos == 2000
data = []
for i in range(0, pairCount):

    num = str(i).zfill(6)
    posFilename = posPrefix + num + ".png"
    negFilename = negPrefix + num + ".png"
    
    im = Image.open("../../data/pedestrianFixed64x128/pos/" + posFilename)
    m = imToMat(im)
    data.append(m)    
#     lowDfeatures, reconFeatures = pca.pca(m.T, 1000)
#     with open("../../data/pedestrianFixed64x128/pos/pca/" + posFilename + ".pca", "wb") as f:
#         pickle.dump(lowDfeatures.T, f)
    
    print  posFilename, " converted"
    
    im = Image.open("../../data/pedestrianFixed64x128/neg/" + negFilename)
    m = imToMat(im)
    data.append(m)
#     lowDfeatures, reconFeatures = pca.pca(m, 1000)
#     with open("../../data/pedestrianFixed64x128/neg/pca/" + negFilename + ".pca", "wb") as f:
#         pickle.dump(lowDfeatures.T, f)
        
    print  negFilename, " converted"
    
lowDfeatures, reconFeatures = pca.pca(np.mat(data), 1000)
    with open("../../data/pedestrianFixed64x128/neg/pca/" + negFilename + ".pca", "wb") as f:
        pickle.dump(lowDfeatures.T, f)
# print np.shape(lowDfeatures);