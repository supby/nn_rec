'''
Created on 10.01.2013

@author: HP625
'''

from nn import BPNN
from shared import DataLoader
import haarExtractor
import utils
print "Load weidths."
dl = DataLoader.DataLoader()
wds = dl.LoadWeidts("../../res/ped_bpnn_tr4000_lr003_reg1_h500_o2_iter3000.dat")

bpnn1 = BPNN.BPNN([5376, 500, 2])
bpnn1.setWeidth(wds)
    
# posPrefix = "pedestrian"
# negPrefix = "not-pedestrian"
posPrefix = "positive-"
negPrefix = "negative-road-"

failures = 0
testPairCount = 50
devideSizes = [(8,4),(16,8),(32,16)]
for i in range(0, testPairCount):
    num = str(i).zfill(6)
    posFilename = posPrefix + num + ".png"
    negFilename = negPrefix + num + ".png"
    
#     iimg = haarExtractor.getIntegral("../../data/pedestrian/pedestrian/" + posFilename)
    iimg = haarExtractor.getIntegral("../../data/pedestrianFixed64x128/pos/" + posFilename)
    features = haarExtractor.extractHaarFeatures(iimg, devideSizes)
    scaled = utils.featureNormalization(features)    
    
    fdt = bpnn1.run(scaled)
    z3t,a3t = fdt[1]
#    print a3t
#     print z3t
    maxV = max(a3t)
    if(0 != a3t.index(maxV)):
        failures = failures + 1
#         print z3t, a3t
    
#     iimg = haarExtractor.getIntegral("../../data/pedestrian/not-pedestrian/" + negFilename)
    iimg = haarExtractor.getIntegral("../../data/pedestrianFixed64x128/neg/" + negFilename)
    features = haarExtractor.extractHaarFeatures(iimg, devideSizes)
    scaled = utils.featureNormalization(features)    
    
    fdt = bpnn1.run(scaled)
    z3t,a3t = fdt[1]
#    print a3t
    maxV = max(a3t)
    if(1 != a3t.index(maxV)):
        failures = failures + 1
        
    print '.'
print "FailuresCount=", failures,"fauluresPer=",float(float(failures)/float(testPairCount*2)*100),"%"
    
    

#iimg = haarExtractor.getIntegral("d:/Dropbox/Dropbox/prj/nn_rec/data/pedestrian/not-pedestrian/not-pedestrian0000.png")
#features = haarExtractor.extractHaarFeatures(iimg)
#scaled = utils.featureNormalization(features)

#bpnn1 = BPNN.BPNN([2432, 50, 2])
#bpnn1.setWeidth(wds)
#fdt = bpnn1.run(scaled)
#z3t,a3t = fdt[1]
#print a3t
#maxV = max(a3t)
#
#print a3t.index(maxV)

