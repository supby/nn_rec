'''
Created on May 2, 2013

@author: asuzanovich
'''

import numpy as np
import utils
import time
from shared import DataLoader
import pickle
from PIL import Image


posPrefix = "positive-"
negPrefix = "negative-road-"

newIndexStart = 1000
pairCount = 1000 # neg + pos == 2000
data = []
for i in range(0, pairCount):

    num = str(i).zfill(6)
    newNum = str(i + newIndexStart).zfill(6)
    posFilename = posPrefix + num + ".png"
    negFilename = negPrefix + num + ".png"
    
    newPosFilename = posPrefix + newNum + ".png"
    newNegFilename = negPrefix + newNum + ".png"
    
    im = Image.open("../../data/pedestrianFixed64x128/pos/" + posFilename)
    im2 = im.transpose(Image.FLIP_LEFT_RIGHT)
    im2.save("../../data/pedestrianFixed64x128/pos/" + newPosFilename, "PNG")
    
    print  posFilename, " converted"
    
    im = Image.open("../../data/pedestrianFixed64x128/neg/" + negFilename)
    im2 = im.transpose(Image.FLIP_LEFT_RIGHT)
    im2.save("../../data/pedestrianFixed64x128/neg/" + newNegFilename, "PNG")
        
    print  negFilename, " converted"
    
