'''
Created on 08.01.2013

@author: HP625
'''
import numpy as np
import math

def featureNormalizationMat(data):
    mat = np.mat(data, np.float)
    minV = np.min(mat)
    maxV = np.max(mat)
    r = maxV - minV    
    
    return np.divide(np.subtract(mat, minV), r)

def featureNormalization(featureVector):
    maxV = np.max(featureVector)
    minV = np.min(featureVector)
    r = maxV - minV
    
    l = len(featureVector)
    scaledVector = np.zeros((l), np.float)
    for i in range(0,l):
        scaledVector[i] = float((featureVector[i] - minV))/r
        
    return scaledVector

def featureStandardization(featureVector):
    l = len(featureVector)
    mean = np.mean(featureVector)
    s = 0
    for i in range(0, l):
        s = s + (featureVector - mean)**2
    dev = math.sqrt(s/l)
    
    scaledVector = np.zeros((l), np.float)
    for i in range(0, l):
        scaledVector[i] = featureVector[i] - dev
        
    return scaledVector
