'''
Created on 03.01.2013

@author: HP625
'''

from PIL import Image
import numpy as np
import time
import pickle
import utils


class HaarFeatures:
    integralImage = None
    haarSize = (0,0)
    featureRectIndex = (0,0)
    
    def __init__(self, integralImage, haarSize, featureRectIndex):
        self.integralImage = integralImage
        self.haarSize = haarSize
        self.featureRectIndex = featureRectIndex
    
    def minIndex(self):
        i = self.featureRectIndex[0]
        j = self.featureRectIndex[1]
        haarH = self.haarSize[0]
        haarW = self.haarSize[1]
        return (i*haarH - 1, j*haarW - 1)
        
    def a(self):
        minIndex = self.minIndex()
        i = self.featureRectIndex[0]
        j = self.featureRectIndex[1]
        return 0 if (i == 0 or j == 0) else self.integralImage[minIndex[0], minIndex[1]]
    def b(self):
        minIndex = self.minIndex()
        i = self.featureRectIndex[0]
        return 0 if(i == 0) else self.integralImage[minIndex[0], minIndex[1] + self.haarSize[1]]
    def c(self):
        minIndex = self.minIndex()
        j = self.featureRectIndex[1]
        return 0 if(j == 0) else self.integralImage[minIndex[0] + self.haarSize[0], minIndex[1]]
    def d(self):
        minIndex = self.minIndex()
        return self.integralImage[minIndex[0] + self.haarSize[0], minIndex[1] + self.haarSize[1]]
        
    def edgeVertical(self):
        haarH = self.haarSize[0]
        haarW = self.haarSize[1]
        minIndex = self.minIndex()
        minX = minIndex[1]
        minY = minIndex[0]
        
        a = self.a()
        b = self.b()
        c = self.c()
        d = self.d()
        
        i = self.featureRectIndex[0]
        e = 0 if(i == 0) else self.integralImage[minY, minX + haarW/2]
        f = self.integralImage[minY + haarH, minX + haarW/2]
        
        return 2*f + a - 2*e - c - d + b
    
    def edgeHorizontal(self):
        haarH = self.haarSize[0]
        haarW = self.haarSize[1]
        minIndex = self.minIndex()
        minX = minIndex[1]
        minY = minIndex[0]
        
        a = self.a()
        b = self.b()
        c = self.c()
        d = self.d()
        
        j = self.featureRectIndex[1]
        e = 0 if(j == 0) else self.integralImage[minY + haarH/2, minX]
        f = self.integralImage[minY + haarH/2, minX + haarW]
        
        return 2*f + a - b - 2*e - d + c
    
    def lineSlimVertical(self):
        haarH = self.haarSize[0]
        haarW = self.haarSize[1]
        minIndex = self.minIndex()
        minX = minIndex[1]
        minY = minIndex[0]
        
        a = self.a()
        b = self.b()
        c = self.c()
        d = self.d()
        
        i = self.featureRectIndex[0]
        f = 0 if(i == 0) else self.integralImage[minY, minX + haarW/3]
        e = 0 if(i == 0) else self.integralImage[minY, minX + (2*haarW)/3]
        n = self.integralImage[minY + haarH, minX + haarW/3]
        m = self.integralImage[minY + haarH, minX + (2*haarW)/3]
        
        return a + 2*n - 2*f - c + 2*e + d - b - 2*m
    
    def lineFatVertical(self):
        haarH = self.haarSize[0]
        haarW = self.haarSize[1]
        minIndex = self.minIndex()
        minX = minIndex[1]
        minY = minIndex[0]
        
        a = self.a()
        b = self.b()
        c = self.c()
        d = self.d()
        
        i = self.featureRectIndex[0]
        f = 0 if(i == 0) else self.integralImage[minY, minX + haarW/4]
        e = 0 if(i == 0) else self.integralImage[minY, minX + (3*haarW)/4]
        n = self.integralImage[minY + haarH, minX + haarW/4]
        m = self.integralImage[minY + haarH, minX + (3*haarW)/4]
        
        return a + 2*n - 2*f - c + 2*e + d - b - 2*m
    
    def lineSlimHorizontal(self):
        haarH = self.haarSize[0]
        haarW = self.haarSize[1]
        minIndex = self.minIndex()
        minX = minIndex[1]
        minY = minIndex[0]
        
        a = self.a()
        b = self.b()
        c = self.c()
        d = self.d()
        
        j = self.featureRectIndex[1]
        f = 0 if(j == 0) else self.integralImage[minY + haarH/3, minX]
        e = 0 if(j == 0) else self.integralImage[minY + (2*haarH)/3, minX]
        n = self.integralImage[minY + haarH/3, minX + haarW]
        m = self.integralImage[minY + (2*haarH)/3, minX + haarW]
        
        return 2*n + a - b - 2*f + 2*e + d - 2*m - c
    
    def lineFatHorizontal(self):
        haarH = self.haarSize[0]
        haarW = self.haarSize[1]
        minIndex = self.minIndex()
        minX = minIndex[1]
        minY = minIndex[0]
        
        a = self.a()
        b = self.b()
        c = self.c()
        d = self.d()
        
        j = self.featureRectIndex[1]
        f = 0 if(j == 0) else self.integralImage[minY + haarH/4, minX]
        e = 0 if(j == 0) else self.integralImage[minY + (3*haarH)/4, minX]
        n = self.integralImage[minY + haarH/4, minX + haarW]
        m = self.integralImage[minY + (3*haarH)/4, minX + haarW]
        
        return 2*n + a - b - 2*f + 2*e + d - 2*m - c
    
    def squareCenter(self):
        haarH = self.haarSize[0]
        haarW = self.haarSize[1]
        minIndex = self.minIndex()
        minX = minIndex[1]
        minY = minIndex[0]
        
        a = self.a()
        b = self.b()
        c = self.c()
        d = self.d()
        
        f = self.integralImage[minY + haarH/3, minX + haarW/3]
        e = self.integralImage[minY + haarH/3, minX + (2*haarW)/3]
        n = self.integralImage[minY + (2*haarH)/3, minX + haarW/3]
        m = self.integralImage[minY + (2*haarH)/3, minX + (2*haarW)/3]
        
        return d + a - b - c - 2*(m + f - e - n)
    
    def squareDiagonal(self):
        haarH = self.haarSize[0]
        haarW = self.haarSize[1]
        minIndex = self.minIndex()
        minX = minIndex[1]
        minY = minIndex[0]
        
        a = self.a()
        b = self.b()
        c = self.c()
        d = self.d()
        
        i = self.featureRectIndex[0]
        j = self.featureRectIndex[1]
        
        f = 0 if(i == 0) else self.integralImage[minY, minX + haarW/2]
        e = self.integralImage[minY + haarH/2, minX + haarW]
        m = self.integralImage[minY + haarH, minX + haarW/2]
        n = 0 if(j == 0) else self.integralImage[minY + haarH/2, minX]
        o = self.integralImage[minY + haarH/2, minX + haarW/2]
        
        return a + 4*o - 2*f - 2*n + d - 2*e - 2*m + b + c
        
        

def getIntegral(filename):
#    print "calculate integral"
#    start = time.time()
    # calculate integral veiw of img
    im = Image.open(filename)
    pix = im.load()
    w,h = im.size
    
    iimg = np.zeros((h,w), np.long)
    for i in range(0, h):    
        for j in range(0, w):
            m = 0 if (i == 0 or j == 0) else iimg[i-1,j-1]
            l = 0 if (j == 0) else iimg[i,j-1]
            t = 0 if (i == 0) else iimg[i-1,j]
            iimg[i,j] = (l + t - m) + pix[j,i]        
            
#    print "Done. ",(time.time() - start)," s."
    
    return iimg

def extractHaarFeatures(integralImage, devideSizes):
#    print "extract haar features"
#    start = time.time()
    FEATURES_TYPE_COUNT = 8
#     FEATURES_TYPE_COUNT = 6 # without diagonal
#    devideSizes = [(8,4),(12,6),(20,10)]
#    devideSizes = [(59,29)]
    fSize = 0
    for devideSize in enumerate(devideSizes):
        ds_t = devideSize[1]        
        fSize = fSize + (ds_t[0]*ds_t[1]*FEATURES_TYPE_COUNT) 
    
    features = np.zeros((fSize), np.long)
    fsPrev = 0
    (h,w) = np.shape(integralImage)
    for devideSize in enumerate(devideSizes):
        devideSize_t = devideSize[1]        
        dh = devideSize_t[0]
        dw = devideSize_t[1]
        fs = dh*dw*FEATURES_TYPE_COUNT
        haarH = h/dh
        haarW = w/dw
        features_ds = np.zeros((fs), np.long)
        c = 0
        for i in range(0, dh):
            for j in range(0, dw):
                haarFeatures = HaarFeatures(integralImage, (haarH,haarW), (i,j))
                features_ds[c] = haarFeatures.edgeVertical()
                c = c + 1
                features_ds[c] = haarFeatures.edgeHorizontal()
                c = c + 1
                features_ds[c] = haarFeatures.lineSlimVertical()
                c = c + 1
                features_ds[c] = haarFeatures.lineFatVertical()
                c = c + 1
                features_ds[c] = haarFeatures.lineSlimHorizontal()
                c = c + 1
                features_ds[c] = haarFeatures.lineFatHorizontal()
                c = c + 1
                features_ds[c] = haarFeatures.squareCenter()
                c = c + 1
                features_ds[c] = haarFeatures.squareDiagonal()
                c = c + 1
        
        features[fsPrev:fsPrev + fs] = features_ds[:]
        fsPrev = fsPrev + fs
        
#    print "Done. ",(time.time() - start)," s."
    return features

#iimg = getIntegral("d:/Dropbox/Dropbox/prj/nn_rec/data/pedestrian/pedestrian/pedestrian0000.png")
#features = extractHaarFeatures(iimg)
#print features
#scaled = utils.featureNormalization(features)
#print scaled
#with open("d:/Dropbox/Dropbox/prj/nn_rec/data/pedestrian/pedestrian/pre/pedestrian0000.png.int", "wb") as f:    
#    pickle.dump(features, f)
#
#with open("d:/Dropbox/Dropbox/prj/nn_rec/data/pedestrian/pedestrian/pre/pedestrian0000.int", "rb") as f:
#    features_f = pickle.load(f)
#
#
#print features_f   
#print len(features_f)
                 
    
    
    
    
    



