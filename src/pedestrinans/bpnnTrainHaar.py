'''
Created on 03.01.2013

@author: HP625
'''

from nn import BPNN
import haarExtractor
import numpy as np
import utils
import time
from shared import DataLoader
import pickle

if __name__ == '__main__':
    pass


posPrefix = "positive-"
negPrefix = "negative-road-"

pairCount = 200 # neg + pos == 4000

print "fill train data"
start = time.time()

rawData = []
labelsData = []
for i in range(0, pairCount):
    num = str(i).zfill(6)
    posFilename = posPrefix + num + ".png"
    negFilename = negPrefix + num + ".png"
    
    with open("../../data/pedestrianFixed64x128/pos/pre/" + posFilename + ".int", "rb") as f:    
        features = pickle.load(f)
    rawData.append(features)
    labelsData.append([1,0])
    
    with open("../../data/pedestrianFixed64x128/neg/pre/" + negFilename + ".int", "rb") as f:    
        features = pickle.load(f)
    rawData.append(features)
    labelsData.append([0,1])
    
print "Done. ",(time.time() - start)," s."

print "Leraning..."
start = time.time()
featuresCount = len(rawData[0])
print "FeaturesCount=",featuresCount
options = BPNN.BPNNOptions()
options.useAdaptiveLearaningRate = False
options.adaptiveLRateStep = 0.001

bpnn1 = BPNN.BPNN([featuresCount, 500, 2], options)
bpnn1.learn(rawData, labelsData, maxIter=3000, desiredErr=0.5, learnRate=0.03, lam=1)

print "Done. ",(time.time() - start)," s."

dl = DataLoader.DataLoader()
dl.SaveWeidths("../../res/ped_bpnn_tr4000_lr003_reg1_h500_o2_iter3000.dat", bpnn1.getWeidths())

