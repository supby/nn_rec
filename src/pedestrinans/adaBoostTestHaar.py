'''
Created on 10.01.2013

@author: HP625
'''
import pickle
import haarExtractor
import utils
from adaboost import adaboostLib

with open("../../res/ped_boost_tr2000_i50_64x128.dat", "rb") as f:
    weakClassArr = pickle.load(f)
#print  weakClassArr
#posPrefix = "pedestrian"
#negPrefix = "not-pedestrian"
posPrefix = "positive-"
negPrefix = "negative-road-"

failures = 0
testPairCount = 100
devideSizes = [(8,4),(16,8),(32,16)]
for i in range(0, testPairCount):
#    num = str(i).zfill(4)
    num = str(i).zfill(6)
    posFilename = posPrefix + num + ".png"
    negFilename = negPrefix + num + ".png"    

    iimg = haarExtractor.getIntegral("../../data/pedestrianFixed64x128/pos/" + posFilename)
    features = haarExtractor.extractHaarFeatures(iimg, devideSizes)
    scaled = utils.featureNormalization(features)    
    
    predictVal = adaboostLib.adaClassify(scaled, weakClassArr)[0]
    if(predictVal != 1):
        failures = failures + 1    

    iimg = haarExtractor.getIntegral("../../data/pedestrianFixed64x128/neg/" + negFilename)
    features = haarExtractor.extractHaarFeatures(iimg, devideSizes)
    scaled = utils.featureNormalization(features)
    
    predictVal = adaboostLib.adaClassify(scaled, weakClassArr)[0]
    if(predictVal != 0):
        failures = failures + 1

print "FailuresCount=", failures,"fauluresPer=",float(float(failures)/float(testPairCount*2)*100),"%"
    
#iimg = haarExtractor.getIntegral("d:/Dropbox/Dropbox/prj/nn_rec/data/pedestrian/pedestrian/pedestrian0000.png")
#features = haarExtractor.extractHaarFeatures(iimg)
#scaled = utils.featureNormalization(features)
#
#print weakClassArr
#predictVal = adaboostLib.adaClassify(scaled, weakClassArr)[0]
#print predictVal 
