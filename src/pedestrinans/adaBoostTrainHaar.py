'''
Created on 09.01.2013

@author: HP625
'''


import numpy as np
import utils
import time
from adaboost import adaboostLib
import pickle

if __name__ == '__main__':
    pass


#posPrefix = "pedestrian"
#negPrefix = "not-pedestrian"

posPrefix = "positive-"
negPrefix = "negative-road-"

trainPairCount = 2000 # neg = 1000 + pos = 1000, train data count

print "fill train data"
start = time.time()

rawData = []
labelsData = np.zeros((trainPairCount), np.float)
filePosCounter = 0
for i in range(0, trainPairCount/2):
    num = str(filePosCounter).zfill(6)
    posFilename = posPrefix + num + ".png"

    with open("../../data/pedestrianFixed64x128/pos/pre/" + posFilename + ".int", "rb") as f:    
        features = pickle.load(f)
    rawData.append(features)
    labelsData[i] = 1.0    
    
    filePosCounter += 1

fileNeqCounter = 0
for i in range(trainPairCount/2, trainPairCount):
    num = str(fileNeqCounter).zfill(6)
    negFilename = negPrefix + num + ".png"

    with open("../../data/pedestrianFixed64x128/neg/pre/" + negFilename + ".int", "rb") as f:    
        features = pickle.load(f)
    rawData.append(features)
    labelsData[i] = 0
    
    fileNeqCounter += 1
    
print "Done. ",(time.time() - start)," s."

print "Feature scaling ..."
start = time.time()

scaledData = utils.featureNormalizationMat(rawData)

print "Done. ",(time.time() - start)," s."

print "Leraning..."
start = time.time()

weakClassArr = adaboostLib.adaBoostTrainDS(scaledData, labelsData.T, numIt=50)

print "Done. ",(time.time() - start)," s."    
    
with open("../../res/ped_boost_tr2000_i50_64x128.dat", "wb") as f:
    pickle.dump(weakClassArr, f)


