'''
Created on 11.01.2013

@author: HP625
'''

import numpy as np
import time
from PIL import Image
from kNN import kNN
import pickle
import haarExtractor

if __name__ == '__main__':
    pass


posPrefix = "positive-"
negPrefix = "negative-road-"

trainCount = 1950 # neg  + pos

print "load data"
start = time.time()

rawData = []
labelsData = np.zeros((trainCount), np.byte)
filePairCounter = 0
for i in xrange(0, trainCount, 2):
    num = str(filePairCounter).zfill(6)
    posFilename = posPrefix + num + ".png"
    negFilename = negPrefix + num + ".png"    
    
    #im = Image.open("../../data/pedestrianFixed64x128/pos/" + posFilename)
    #rawData.append(list(im.getdata()))
    with open("../../data/pedestrianFixed64x128/pos/pre/" + posFilename + ".int", "rb") as f:    
        features = pickle.load(f)
    rawData.append(features)
    labelsData[i] = 1    
    
    #im = Image.open("../../data/pedestrianFixed64x128/neg/" + negFilename)
    #rawData.append(list(im.getdata()))
    with open("../../data/pedestrianFixed64x128/neg/pre/" + negFilename + ".int", "rb") as f:    
        features = pickle.load(f)
    rawData.append(features)    
    labelsData[i + 1] = 0
    
    filePairCounter += 1

rawData = np.mat(rawData)

knn = kNN.KNN(rawData, labelsData, 15)

print "Done. ",(time.time() - start)," s."

print "Testing..."
start = time.time()
 
failures = 0
testPairCount = 100
startPair = 0
devideSizes = [(8,4),(16,8),(32,16)]
for i in range(startPair, testPairCount+startPair):
    num = str(i).zfill(4)
    posFilename = 'pedestrian' + num + ".png"
    negFilename = 'not-pedestrian' + num + ".png"     
     
#     im = Image.open("../../data/pedestrianFixed64x128/pos/" + posFilename)
#     scaledRow = list(im.getdata())    
    iimg = haarExtractor.getIntegral("../../data/pedestrian/pedestrian/" + posFilename)
    features = haarExtractor.extractHaarFeatures(iimg, devideSizes)     
    predictVal = knn.classify(features)
    if(predictVal != 1):
        failures = failures + 1
         
#     im = Image.open("../../data/pedestrianFixed64x128/neg/" + negFilename)
#     scaledRow = list(im.getdata()) 
    iimg = haarExtractor.getIntegral("../../data/pedestrian/not-pedestrian/" + negFilename)
    features = haarExtractor.extractHaarFeatures(iimg, devideSizes)
    predictVal = knn.classify(features)
    if(predictVal != 0):
        failures = failures + 1
         
    print "."
     
print "FailuresCount=", failures,"fauluresPer=",float(float(failures)/float(testPairCount*2)*100),"%"
 
print "Done. ",(time.time() - start)," s."   
    

