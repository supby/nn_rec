'''
Created on 14.01.2013

@author: HP625
'''

from nn import BPNN
import haarExtractor
import numpy as np
import utils
import time
from shared import DataLoader
import pickle

#posPrefix = "pedestrian"
#negPrefix = "not-pedestrian"

posPrefix = "positive-"
negPrefix = "negative-road-"

pairCount = 2000 # neg + pos == 4000
devideSizes = [(8,4),(16,8),(32,16)]
for i in range(0, pairCount):
#    num = str(i).zfill(4)
    num = str(i).zfill(6)
    posFilename = posPrefix + num + ".png"
    negFilename = negPrefix + num + ".png"
    
#    iimg = haarExtractor.getIntegral("../../data/pedestrian/pedestrian/" + posFilename)
    iimg = haarExtractor.getIntegral("../../data/pedestrianFixed64x128/pos/" + posFilename)
    features = haarExtractor.extractHaarFeatures(iimg, devideSizes)
#    with open("../../data/pedestrian/pedestrian/pre/" + posFilename + ".int", "wb") as f:    
    with open("../../data/pedestrianFixed64x128/pos/pre/" + posFilename + ".int", "wb") as f:
        pickle.dump(features, f)
    
    print  posFilename, " converted"
    
#    iimg = haarExtractor.getIntegral("../../data/pedestrian/not-pedestrian/" + negFilename)
    iimg = haarExtractor.getIntegral("../../data/pedestrianFixed64x128/neg/" + negFilename)
    features = haarExtractor.extractHaarFeatures(iimg, devideSizes)
#    with open("../../data/pedestrian/not-pedestrian/pre/" + negFilename + ".int", "wb") as f:    
    with open("../../data/pedestrianFixed64x128/neg/pre/" + negFilename + ".int", "wb") as f:
        pickle.dump(features, f)
        
    print  negFilename, " converted"
    
